<?php $page = 'siswa';include '../template/admin/header.php'; ?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Data Siswa</span> 
                </div> 
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <button id="btn-add" class="btn sbold green" data-target="#modal-action" data-toggle="modal"> Add New
                                    <i class="fa fa-plus"></i>
                                </button> 
                            </div>
                        </div> 
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="t_siswa">
                    <thead>
                        <tr>
                            <th class="text-center" width="20px"> No. </th>
                            <th class="text-center"> Nama </th> 
                            <th class="text-center"> NISN </th> 
                            <th class="text-center" width="120px"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <ul id="pagination" class="pagination-sm"></ul>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!--DOC: Aplly "modal-cached" class after "modal" class to enable ajax content caching--> 
<div class="modal fade" id="modal-action" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Data Siswa</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-md-5">
                            <br>
                            <label>Nama </label>
                            <input name="id_siswa" id="id_siswa" type="hidden" class="form-control" placeholder="" readonly="readonly"> 
                            <input name="nama_siswa" id="nama_siswa" type="text" class="form-control" placeholder=""> 
                        </div>
                        <div class="col-md-3">
                            <br>
                            <label>NISN </label> 
                            <input name="nisn" id="nisn" type="text" class="form-control" placeholder=""> 
                        </div>
                        <div class="col-md-2">
                            <br>
                            <label>Jenis Kelamin </label>  
                            <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                <option value="Laki - Laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <br>
                            <label>Agama </label> 
                            <select class="form-control" name="agama" id="agama">
                                <option value="Islam">Islam</option>
                                <option value="Kristen Protestan">Kristen Protestan</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Kong Hu Cu">Kong Hu Cu</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <br>
                            <label>Tempat Lahir </label> 
                            <input name="tempat_lahir" id="tempat_lahir" type="text" class="form-control" placeholder=""> 
                        </div>
                        <div class="col-md-2">
                            <br>
                            <label>Tanggal Lahir </label> 
                            <input class="form-control date-picker" name="tanggal_lahir" id="tanggal_lahir" type="text" value="">
                        </div>
                        <div class="col-md-4">
                            <br>
                            <label>Telephone </label> 
                            <input name="telephone" id="telephone" type="text" class="form-control" placeholder=""> 
                        </div> 
                        <div class="col-md-12">
                            <br>
                            <label>Alamat </label> 
                            <textarea class="form-control" name="alamat" id="alamat"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-action">
                    <button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn green crud-submit">Save changes</button>
                </div> 
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<!--DOC: Modal Detail--> 
<div class="modal fade" id="modal-detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Data Siswa</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Nama </div>
                                <div class="col-xs-9">
                                    <span id="d_nama_siswa"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Nisn </div>
                                <div class="col-xs-9">
                                    <span id="d_nisn"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Jenis Kelamin </div>
                                <div class="col-xs-9">
                                    <span id="d_jenis_kelamin"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Agama </div>
                                <div class="col-xs-9">
                                    <span id="d_agama"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Tempat Lahir </div>
                                <div class="col-xs-9">
                                    <span id="d_tempat_lahir"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Tanggal Lahir </div>
                                <div class="col-xs-9">
                                    <span id="d_tanggal_lahir"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Telephone </div>
                                <div class="col-xs-9">
                                    <span id="d_telephone"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Alamat </div>
                                <div class="col-xs-9">
                                    <span id="d_alamat"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<script type="text/javascript">  

    $( document ).ready(function() {
        datatable = $('#t_siswa').dataTable({
            "bProcessing": true,
            "serverSide": true,
            "ajax": {
                "url": url+'api/getData.php',
                "type": "post",
                "data":{
                    table_info : {
                        table_name : 'siswa', 
                        order_by : 'nama_siswa', 
                        id : 'id_siswa'},
                        column :{
                            0 : 'nama_siswa', 
                            1 : 'nisn', 
                        }},
                    },
            error: function () {  // error handling code
                $("#t_siswa").css("display", "none");
            }
        });
    }); 

    $("#btn-add").click(function(e){
        $("input[type=text], textarea").val("");
        $('.modal-footer-action').empty();
        $('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="create()">Create</button>');
        $('#form-action').attr('onSubmit','create()'); 
    });
    /* Create new Item */
    function create() {  
        // console.log(namas);
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/create.php',
            data:{
                table_name      : 'siswa', 
                order_by        : 'nama_siswa', 
                nama_siswa      : $('[name="nama_siswa"]').val(),
                nisn            : $('[name="nisn"]').val(),
                jenis_kelamin   : $('[name="jenis_kelamin"]').val(),
                agama           : $('[name="agama"]').val(),
                tempat_lahir    : $('[name="tempat_lahir"]').val(),
                tanggal_lahir   : $('[name="tanggal_lahir"]').val().replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2"),
                telephone       : $('[name="telephone"]').val(),
                alamat          : $('[name="alamat"]').val(),
            },
            timeout: 10000,
            success: function (data) {
                if (data.status) {
                    toastr.success(data.pesan, 'Success');
                    datatable.api().ajax.reload(null, false);
                    $(".modal").modal('hide');
                } else {
                    toastr.warning(data.pesan, 'Warning');
                }
            },
            error: function () {
                datatable.api().ajax.reload(null, false);
                toastr.warning('An error occurred while connecting to the server.', 'Warning');
            }
        });
        $("input[type=text], textarea").val("");
    }; 


    /* CUpdate */
    function update(id_siswa) { 
        $('.modal-footer-action').empty();
        $('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="proses_update()">Update</button>');
        $('#form-action').attr('onSubmit','proses_update()');  
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/get_update_data.php',
            data:{
                table_name  : 'siswa', 
                id_siswa    : id_siswa
            },
            success: function (data) {   
                $('[name="id_siswa"]').val(data.id_siswa);
                $('[name="nama_siswa"]').val(data.nama_siswa); 
                $('[name="nisn"]').val(data.nisn);
                $('[name="jenis_kelamin"]').val(data.jenis_kelamin).trigger("change");
                $('[name="agama"]').val(data.agama).trigger("change");
                $('[name="tempat_lahir"]').val(data.tempat_lahir);
                $('[name="tanggal_lahir"]').val(data.tanggal_lahir.replace(/(\d{4})\-(\d{2})\-(\d{2})/, "$2/$3/$1"));
                $('[name="telephone"]').val(data.telephone);
                $('[name="alamat"]').val(data.alamat);
            }
        })
    }; 

    /* Proses Update */
    function proses_update() {   
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/update.php',
            data:{
                table_name      : 'siswa', 
                order_by        : 'nama_siswa', 
                id_siswa        : $('[name="id_siswa"]').val(), 
                nama_siswa      : $('[name="nama_siswa"]').val(),
                nisn            : $('[name="nisn"]').val(),
                jenis_kelamin   : $('[name="jenis_kelamin"]').val(),
                agama           : $('[name="agama"]').val(),
                tempat_lahir    : $('[name="tempat_lahir"]').val(),
                tanggal_lahir   : $('[name="tanggal_lahir"]').val().replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2"),
                telephone       : $('[name="telephone"]').val(),
                alamat          : $('[name="alamat"]').val(),
            },
            timeout: 10000,
            success: function (data) {
                if (data.status) {
                    toastr.success(data.pesan, 'Success');
                    datatable.api().ajax.reload(null, false);
                    $(".modal").modal('hide');
                } else {
                    toastr.warning(data.pesan, 'Galat');
                }
            },
            error: function () {
                datatable.api().ajax.reload(null, false);
                toastr.warning('An error occurred while connecting to the server.', 'Galat');
            }
        }); 
        $("input[type=text], textarea").val("");
    }; 


    /* Remove Item */
    function delete_data(id_siswa) {
        var delete_data=confirm('Are you sure to delete this data?');
        if (delete_data) {  
            var form_action = 'api/delete.php'; 
            var table_name = 'siswa'; 

            $.ajax({
                dataType: 'json',
                type:'POST',
                url: url + form_action,
                data:{table_name : table_name, order_by : 'nama_siswa', id_siswa:id_siswa},
                timeout: 10000,
                success: function (data) {
                    if (data.status) {
                        toastr.success(data.pesan, 'Success');
                        datatable.api().ajax.reload(null, false);
                        $(".modal").modal('hide');
                    } else {
                        toastr.warning(data.pesan, 'Galat');
                    }
                },
                error: function () {
                    datatable.api().ajax.reload(null, false);
                    toastr.warning('An error occurred while connecting to the server.', 'Galat');
                }
            })

        }
    }; 

    /* CUpdate */
    function detail(id_siswa) {    
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/get_update_data.php',
            data:{
                table_name : 'siswa', 
                id_siswa:id_siswa},
                success: function (data) {  
                $('#d_nama_siswa').html(data.nama_siswa); 
                $('#d_nisn').html(data.nisn);
                $('#d_jenis_kelamin').html(data.jenis_kelamin);
                $('#d_agama').html(data.agama);
                $('#d_tempat_lahir').html(data.tempat_lahir);
                $('#d_tanggal_lahir').html(moment(data.tanggal_lahir).format('DD-MMMM-YYYY'));
                $('#d_telephone').html(data.telephone);
                $('#d_alamat').html(data.alamat);
            }
        })
    }; 
</script>

<?php include '../template/admin/footer.php'; ?>