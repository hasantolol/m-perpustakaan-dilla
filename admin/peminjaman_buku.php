<?php $page = 'peminjaman_buku';include '../template/admin/header.php'; ?>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" icon-layers font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Peminjaman Buku</span> 
				</div> 
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group">
								<button id="btn-add" class="btn sbold green" data-target="#modal-action" data-toggle="modal"> Add New
									<i class="fa fa-plus"></i>
								</button> 
							</div>
						</div> 
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="t_peminjaman">
					<thead>
						<tr>
							<th class="text-center" width="20px"> No. </th>
							<th class="text-center"> Nomor Pinjam </th> 
							<th class="text-center"> Nama Siswa </th> 
							<th class="text-center"> Jumlah Pinjaman </th> 
							<th class="text-center" width="120px"> Action </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul id="pagination" class="pagination-sm"></ul>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!--DOC: Aplly "modal-cached" class after "modal" class to enable ajax content caching--> 
<div class="modal fade" id="modal-action" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Data Peminjaman</h4>
				</div>
				<div class="modal-body"> 
					<div class="row" id = "content">
						<div class="col-md-3">
							<br>
							<label>Nomor Peminjaman </label>
							<input name="id_peminjaman" id="id_peminjaman" type="hidden" class="form-control" placeholder="" readonly="readonly"> 
							<input name="nomor_pinjam" id="nomor_pinjam" type="text" class="form-control" placeholder=""> 
						</div>
						<div class="col-md-2">
							<br>
							<label>Tanggal Pinjam</label> 
							<input class="form-control date-picker" name="tanggal_pinjam" id="tanggal_pinjam" data-date-format="yyyy-mm-dd" type="text" value="" readonly>
						</div>
						<div class="col-md-2">
							<br>
							<label>Lama Pinjam</label> 
							<input class="form-control" name="lama_pinjam" id="lama_pinjam" type="number" value="">
						</div>
						<div class="col-md-5">
							<br>
							<label>Siswa </label> 
							<select class="form-control select2" name="id_siswa" id="id_siswa" >
							</select>
						</div>
						<div class="col-md-12">
							<br>
							<label>Keterangan </label> 
							<textarea class="form-control" name="keterangan" id="keterangan"></textarea>
						</div>
						<input type="hidden" name="status" id="status" value="ashdshadgasdg" readonly>
						<input type="hidden" name="id_user" id="id_user" readonly>
					</div>
					<div class="row">
						<div class=" col-md-12">
							<br> 
							<table width="100%"  class="table table-striped table-hover dt-responsive " id="table-peminjaman"> 
								<thead>
									<tr bgcolor="#3a475b" style="color : white;">
										<th class="text-center" width="">Judul Peminajaman</th>
										<th class="text-center" width="80px">Jumlah</th>
										<th class="text-center" width="80px" ></th>
									</tr>
								</thead>
								<tbody class="table-peminjaman-body">
								</tbody>
							</table> 
						</div> 
						<div class="col-md-12">
							<button type="button"  class="btn btn-success" onclick="addRow()">Add Book <i class="fa fa-plus"></i></button>
						</div> 
					</div>
				</div>
				<div class="modal-footer modal-footer-action">
					<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn green crud-submit">Save changes</button>
				</div> 
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div> 

<!--DOC: Modal Detail--> 
<div class="modal fade" id="modal-detail" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal" id="form-actions" action="javascript:void(0);" onSubmit="create()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Data Peminjaman</h4>
				</div>
				<div class="modal-body">
					<div class="form-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-xs-3 ">Nomor Pinjam </div>
								<div class="col-xs-9">
									<span id="d_nomor_pinjam"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Tanggal Pinjam </div>
								<div class="col-xs-9">
									<span id="d_tanggal_pinjam"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Lama Pinjam </div>
								<div class="col-xs-9">
									<span id="d_lama_pinjam"></span> Hari
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Nama Siswa </div>
								<div class="col-xs-9">
									<span id="d_nama_siswa"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Status </div>
								<div class="col-xs-9">
									<span id="d_status"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Petugas </div>
								<div class="col-xs-9">
									<span id="d_nama_user"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Keterangan </div>
								<div class="col-xs-9">
									<span id="d_keterangan"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div> 
<script type="text/javascript">  

	$( document ).ready(function() {

		datatable = $('#t_peminjaman').dataTable({
			"bProcessing": true,
			"serverSide": true,
			"ajax": {
				"url": url+'api/getData.php',
				"type": "post",
				"data":{
					table_info : {
						table_name : 'peminjaman', 
						order_by : 'tanggal_pinjam', 
						id : 'id_peminjaman',
					},
					column :{
						0 : 'nomor_pinjam', 
						1 : 'nama_siswa', 
						2 : 'pinjaman', 
					},
					join :{
						0:{
							table_name 	: '( SELECT SUM(jumlah) AS pinjaman, id_peminjaman FROM peminjaman_item GROUP BY id_peminjaman )tmp_peminjaman_item', 
							condition 	: ' LEFT JOIN ( SELECT SUM(jumlah) AS pinjaman, id_peminjaman FROM peminjaman_item GROUP BY id_peminjaman )tmp_peminjaman_item ON tmp_peminjaman_item.id_peminjaman = peminjaman.id_peminjaman',
							column 		: 'pinjaman' 
						},
						1:{
							table_name 	: 'siswa', 
							condition 	: ' LEFT JOIN siswa ON siswa.id_siswa = peminjaman.id_siswa',
							column 		: 'nama_siswa' 
						}
					}
				},
			},
            error: function () {  // error handling code
            	$("#t_peminjaman").css("display", "none");
            }
        });

		$(".modal").on("hidden.bs.modal", function(){
			$(".table-peminjaman-body").html("");
		});

		$('#id_siswa').select2({
			tags: false,
			dropdownParent: $("#modal-action"),
			width: '100%', 
			placeholder: "-- Pilih Siswa --",
			ajax : {
				url: url + 'api/get_select2.php',
				dataType: 'json',
				type: 'post',
				delay: 250,
				data: function(params) {
        			// console.log(params);
        			return {
        				table_name : 'siswa',
        				kolom_search : 'nama_siswa',
        				id : 'id_siswa',
        				text : 'nama_siswa',
        				q: params.term || '', // search term
        				page_limit: 10,
        				page: params.page || 1
        			};
        		},
        		processResults: function(data, params) {
        			return {
        				results: data.items,
        				'pagination': {
        					'more': data.more
        				}
        			};
        		},
        		cache: true,
        	}
        });
	}); 

	$("#btn-add").click(function(e){
		$("input[type=text], textarea").val("");
		$('.modal-footer-action').empty();
		$('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="create()">Create</button>');
		$('#form-action').attr('onSubmit','create()'); 
		$('#status').val('borrowed');
		$('#id_user').val('0');
		$('#lama_pinjam').val('7');
		$('#tanggal_pinjam').val(moment().format("YYYY-MM-DD"));
	});
	/* Create new Item */
	function create() {  
		// console.log(namas);
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/create_peminjaman.php',
			data:{
				table_name      : 'peminjaman', 
				order_by        : 'judul', 
				data 			: $('#content :input').serialize(),
				table_batch_name: 'peminjaman_item', 
				batch 			: $('#table-peminjaman :input').serialize(),
			},
			timeout: 10000,
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan, 'Success');
					datatable.api().ajax.reload(null, false);
					$(".modal").modal('hide');
					$("input[type=text], textarea, option, input[type=number]").val("");
				} else {
					toastr.warning(data.pesan, 'Warning');
				}
			},
			error: function () {
				datatable.api().ajax.reload(null, false);
				toastr.warning('An error occurred while connecting to the server.', 'Warning');
			}
		});
	}; 


	/* CUpdate */
	function update(id_peminjaman) { 
		$('.modal-footer-action').empty();
		$('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="proses_update()">Update</button>');
		$('#form-action').attr('onSubmit','proses_update()');  
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/get_update_data.php',
			data:{
				table_name  	: 'peminjaman', 
				id_peminjaman  	: id_peminjaman,
				join 			: {
					0:{
						table_name 	: '( SELECT SUM(jumlah) AS pinjaman, id_peminjaman FROM peminjaman_item GROUP BY id_peminjaman )tmp_peminjaman_item', 
						condition 	: ' LEFT JOIN ( SELECT SUM(jumlah) AS pinjaman, id_peminjaman FROM peminjaman_item GROUP BY id_peminjaman )tmp_peminjaman_item ON tmp_peminjaman_item.id_peminjaman = peminjaman.id_peminjaman',
						column 		: 'pinjaman' 
					},
					1:{
						table_name 	: 'siswa', 
						condition 	: ' LEFT JOIN siswa ON siswa.id_siswa = peminjaman.id_siswa',
						column 		: 'nama_siswa' 
					},
					2:{
						table_name 	: 'user', 
						condition 	: ' LEFT JOIN user ON user.id_user = peminjaman.id_user',
						column 		: 'nama_user' 
					}
				},
				batch 			: {
					table_name 	: 'peminjaman_item',
					column 		: 'id_peminjaman',
					join 		: {
						0:{
							condition	: ' LEFT JOIN buku on buku.id_buku = peminjaman_item.id_buku',
							column 		: 'judul'
						}
					}
				}
			},
			success: function (data) { 
				$('#id_peminjaman').val(data.data.id_peminjaman);
				$('#nomor_pinjam').val(data.data.nomor_pinjam);
				$('#tanggal_pinjam').val(moment(data.data.tanggal_pinjam).format('YYYY-MM-DD')); 
				$('#id_siswa').append('<option value="'+data.data.id_siswa+'">'+data.data.nama_siswa+'</option>');
				$('#id_siswa').val(data.data.id_siswa).trigger("change");
				$('#keterangan').val(data.data.keterangan);
				$('#lama_pinjam').val(data.data.lama_pinjam);
				$('#status').val(data.data.status);
				$('#id_user').val(data.data.id_user);
				
				$.each(data.batch, function(index, val) {
					addRow(val);
				}); 
			}
		})
	}; 

	/* Proses Update */
	function proses_update() {   
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/update.php',
			data:{
				table_name      : 'peminjaman', 
				order_by        : 'judul', 
				data 			: $('#content :input').serialize(),
				table_batch_name: 'peminjaman_item', 
				batch 			: $('#table-peminjaman :input').serialize(),
			},
			timeout: 10000,
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan, 'Success');
					datatable.api().ajax.reload(null, false);
					$(".modal").modal('hide');
					$("input[type=text], textarea, option, input[type=number]").val("");
				} else {
					toastr.warning(data.pesan, 'Galat');
				}
			},
			error: function () {
				datatable.api().ajax.reload(null, false);
				toastr.warning('An error occurred while connecting to the server.', 'Galat');
			}
		}); 
	}; 


	/* Remove Item */
	function delete_data(id_peminjaman) {
		var delete_data=confirm('Are you sure to delete this data?');
		if (delete_data) {  
			var form_action = 'api/delete.php'; 

			$.ajax({
				dataType: 'json',
				type:'POST',
				url: url + form_action,
				data:{
					table_name 			: 'peminjaman',  
					id					: id_peminjaman,
					table_name_batch 	: 'peminjaman_item'
				},
				timeout: 10000,
				success: function (data) {
					if (data.status) {
						toastr.success(data.pesan, 'Success');
						datatable.api().ajax.reload(null, false);
						$(".modal").modal('hide');
					} else {
						toastr.warning(data.pesan, 'Galat');
					}
				},
				error: function () {
					datatable.api().ajax.reload(null, false);
					toastr.warning('An error occurred while connecting to the server.', 'Galat');
				}
			})

		}
	}; 

	/* CUpdate */
	function detail(id_peminjaman) {    
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/get_update_data.php',
			data:{
				table_name  	: 'peminjaman', 
				id_peminjaman  	: id_peminjaman,
				join 			: {
					0:{
						table_name 	: '( SELECT SUM(jumlah) AS pinjaman, id_peminjaman FROM peminjaman_item GROUP BY id_peminjaman )tmp_peminjaman_item', 
						condition 	: ' LEFT JOIN ( SELECT SUM(jumlah) AS pinjaman, id_peminjaman FROM peminjaman_item GROUP BY id_peminjaman )tmp_peminjaman_item ON tmp_peminjaman_item.id_peminjaman = peminjaman.id_peminjaman',
						column 		: 'pinjaman' 
					},
					1:{
						table_name 	: 'siswa', 
						condition 	: ' LEFT JOIN siswa ON siswa.id_siswa = peminjaman.id_siswa',
						column 		: 'nama_siswa' 
					},
					2:{
						table_name 	: 'user', 
						condition 	: ' LEFT JOIN user ON user.id_user = peminjaman.id_user',
						column 		: 'nama_user' 
					}
				},
				batch 			: {
					table_name 	: 'peminjaman_item',
					column 		: 'id_peminjaman',
					join 		: {
						0:{
							condition	: ' LEFT JOIN buku on buku.id_buku = peminjaman_item.id_buku',
							column 		: 'judul'
						}
					}
				}
			},
			success: function (data) {  
				// console.log(data);
				// data.data.tanggal_pinjam = moment(data.data.tanggal_pinjam).format('YYYY-MM-DD');
				$.each(data.data, function(index, val) {
					let label = '#d_'+index;
					if (index == 'tanggal_pinjam') 
						{value = moment(val).format('DD-MMMM-YYYY');}
					else
						{value = val;}
					$(label).html(value);
				}); 
			}
		})
	}; 

	function addRow( val = null){
		var tableID='#table-peminjaman';
		let row = '<tr>';
		row += '<td class=""><input type="text" name="id_peminjaman[]" value="' + getObjectValue(val, 'id_peminjaman') + '" readonly><select class="term form-control id_buku" name="id_buku[]" required>';
		row += '<option value="' + getObjectValue(val, 'id_buku') + '" selected>' + getObjectValue(val, 'judul') + '</option>';
		row += '</select></td>';
		row += '<td class="text-center"><input type="number" min="0" class="form-control select2" name="jumlah[]" value="' + getObjectValue(val, 'jumlah') + '" required/></td>';
		row += '<td class="text-center"><a class="btn btn-circle btn-icon-only btn-default" onclick="removeRow(this)" href="javascript:;" title="Delete"><i class="icon-trash"></i></a></td>';
		row +='</tr>';

		$(tableID).append(row);
		set_select_2();
	}

	function set_select_2(){
		$('.id_buku').select2({
			tags: false,
			dropdownParent: $("#modal-action"),
			width: '100%', 
			placeholder: "-- Pilih Buku --",
			ajax : {
				url: url + 'api/get_select2.php',
				dataType: 'json',
				type: 'post',
				delay: 250,
				data: function(params) {
					return {
						table_name : 'buku',
						kolom_search : 'judul',
						id : 'id_buku',
						text : 'judul',
						q: params.term || '', 
						page_limit: 10,
						page: params.page || 1
					};
				},
				processResults: function(data, params) {
					return {
						results: data.items,
						'pagination': {
							'more': data.more
						}
					};
				},
				cache: true,
			}
		});
	}
	function getObjectValue(obj, selector) {
		if (obj === null) {
			return '';
		}
		return obj[selector];
	}

    // delete data temp table
    function removeRow(ths) { 
    	let tableID='#list-product';
    	var td = $(ths).parent(),
    	tr = td.parent();
    	$(tr).remove();
    }
</script>

<?php include '../template/admin/footer.php'; ?>