<?php $page = 'home';include '../template/admin/header.php'; ?>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" icon-layers font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Sample Datatable</span>
				</div> 
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group">
								<button id="sample_editable_1_new" class="btn sbold green" data-target="#basic" data-toggle="modal"> Add New
									<i class="fa fa-plus"></i>
								</button> 
							</div>
						</div> 
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
					<thead>
						<tr>
							<th class="table-checkbox">
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
									<span></span>
								</label>
							</th>
							<th> Username </th>
							<th> Email </th>
							<th> Status </th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							<td>
								<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
									<input type="checkbox" class="checkboxes" value="1" />
									<span></span>
								</label>
							</td>
							<td> shuxer </td>
							<td>
								<a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
							</td>
							<td>
								<span class="label label-sm label-success"> Approved </span>
							</td>
						</tr> 
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!--DOC: Aplly "modal-cached" class after "modal" class to enable ajax content caching-->
<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<img src="assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
				<span> &nbsp;&nbsp;Loading... </span>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Modal Title</h4>
			</div>
			<div class="modal-body"> Modal body goes here </div>
			<div class="modal-footer">
				<button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
				<button type="button" class="btn green">Save changes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php include '../template/admin/footer.php'; ?>