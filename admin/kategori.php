<?php $page = 'kategori';include '../template/admin/header.php'; ?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Data kategori</span> 
                </div> 
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group">
                                <button id="btn-add" class="btn sbold green" data-target="#modal-action" data-toggle="modal"> Tambah Kategori
                                    <i class="fa fa-plus"></i>
                                </button> 
                            </div>
                        </div> 
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="t_kategori">
                    <thead>
                        <tr>
                            <th class="text-center" width="20px"> No. </th>
                            <th class="text-center"> Kategori </th> 
                            <th class="text-center" width="120px"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <ul id="pagination" class="pagination-sm"></ul>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!--DOC: Aplly "modal-cached" class after "modal" class to enable ajax content caching--> 
<div class="modal fade" id="modal-action" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Data kategori</h4>
                </div>
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-md-5">
                            <br>
                            <label>Kategori </label>
                            <input name="id_kategori" id="id_kategori" type="hidden" class="form-control" placeholder="" readonly="readonly"> 
                            <input name="nama_kategori" id="nama_kategori" type="text" class="form-control" placeholder=""> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-footer-action">
                    <button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn green crud-submit">Save changes</button>
                </div> 
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<!--DOC: Modal Detail--> 
<div class="modal fade" id="modal-detail" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Data Kategori</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-xs-3 ">Kategori </div>
                                <div class="col-xs-9">
                                    <span id="d_nama_kategori"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 
<script type="text/javascript">  

    $( document ).ready(function() {
        datatable = $('#t_kategori').dataTable({
            "bProcessing": true,
            "serverSide": true,
            "ajax": {
                "url": url+'api/getData.php',
                "type": "post",
                "data":{
                    table_info : {
                        table_name : 'kategori', 
                        order_by : 'nama_kategori', 
                        id : 'id_kategori'
                    },
                        column :{
                            0 : 'nama_kategori',  
                        }},
                    },
            error: function () {  // error handling code
                $("#t_kategori").css("display", "none");
            }
        });
    }); 

    $("#btn-add").click(function(e){
        $("input[type=text], textarea").val("");
        $('.modal-footer-action').empty();
        $('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="create()">Create</button>');
        $('#form-action').attr('onSubmit','create()'); 
    });
    /* Create new Item */
    function create() {  
        // console.log(namas);
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/create.php',
            data:{
                table_name      : 'kategori', 
                order_by        : 'nama_kategori', 
                nama_kategori   : $('[name="nama_kategori"]').val(), 
            },
            timeout: 10000,
            success: function (data) {
                if (data.status) {
                    toastr.success(data.pesan, 'Success');
                    datatable.api().ajax.reload(null, false);
                    $(".modal").modal('hide');
                } else {
                    toastr.warning(data.pesan, 'Warning');
                }
            },
            error: function () {
                datatable.api().ajax.reload(null, false);
                toastr.warning('An error occurred while connecting to the server.', 'Warning');
            }
        });
        $("input[type=text], textarea").val("");
    }; 


    /* CUpdate */
    function update(id_kategori) { 
        $('.modal-footer-action').empty();
        $('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="proses_update()">Update</button>');
        $('#form-action').attr('onSubmit','proses_update()');  
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/get_update_data.php',
            data:{
                table_name  : 'kategori', 
                id_kategori : id_kategori
            },
            success: function (data) {   
                $('[name="id_kategori"]').val(data.id_kategori);
                $('[name="nama_kategori"]').val(data.nama_kategori);  
            }
        })
    }; 

    /* Proses Update */
    function proses_update() {   
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/update.php',
            data:{
                table_name      : 'kategori', 
                order_by        : 'nama_kategori', 
                id_kategori     : $('[name="id_kategori"]').val(), 
                nama_kategori   : $('[name="nama_kategori"]').val(),
            },
            timeout: 10000,
            success: function (data) {
                if (data.status) {
                    toastr.success(data.pesan, 'Success');
                    datatable.api().ajax.reload(null, false);
                    $(".modal").modal('hide');
                } else {
                    toastr.warning(data.pesan, 'Galat');
                }
            },
            error: function () {
                datatable.api().ajax.reload(null, false);
                toastr.warning('An error occurred while connecting to the server.', 'Galat');
            }
        }); 
        $("input[type=text], textarea").val("");
    }; 


    /* Remove Item */
    function delete_data(id_kategori) {
        var delete_data=confirm('Are you sure to delete this data?');
        if (delete_data) {  
            var form_action = 'api/delete.php'; 
            var table_name = 'kategori'; 

            $.ajax({
                dataType: 'json',
                type:'POST',
                url: url + form_action,
                data:{
                    table_name  : table_name, 
                    order_by    : 'nama_kategori', 
                    id_kategori : id_kategori
                },
                timeout: 10000,
                success: function (data) {
                    if (data.status) {
                        toastr.success(data.pesan, 'Success');
                        datatable.api().ajax.reload(null, false);
                        $(".modal").modal('hide');
                    } else {
                        toastr.warning(data.pesan, 'Galat');
                    }
                },
                error: function () {
                    datatable.api().ajax.reload(null, false);
                    toastr.warning('An error occurred while connecting to the server.', 'Galat');
                }
            })

        }
    }; 

    /* CUpdate */
    function detail(id_kategori) {    
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: url + 'api/get_update_data.php',
            data:{
                table_name  : 'kategori', 
                id_kategori :id_kategori},
                success: function (data) {  
                $('#d_nama_kategori').html(data.nama_kategori);  
            }
        })
    }; 
</script>

<?php include '../template/admin/footer.php'; ?>