<?php $page = 'buku';include '../template/admin/header.php'; ?>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" icon-layers font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Data Buku</span> 
				</div> 
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group">
								<button id="btn-add" class="btn sbold green" data-target="#modal-action" data-toggle="modal"> Add New
									<i class="fa fa-plus"></i>
								</button> 
							</div>
						</div> 
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover table-checkable order-column" id="t_buku">
					<thead>
						<tr>
							<th class="text-center" width="20px"> No. </th>
							<th class="text-center"> Judul </th> 
							<th class="text-center"> Kategori </th> 
							<th class="text-center"> Penerbit </th> 
							<th class="text-center" width="120px"> Action </th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul id="pagination" class="pagination-sm"></ul>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!--DOC: Aplly "modal-cached" class after "modal" class to enable ajax content caching--> 
<div class="modal fade" id="modal-action" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Data buku</h4>
				</div>
				<div class="modal-body"> 
					<div class="row">
						<div class="col-md-6">
							<br>
							<label>Judul </label>
							<input name="id_buku" id="id_buku" type="hidden" class="form-control" placeholder="" readonly="readonly"> 
							<input name="judul" id="judul" type="text" class="form-control" placeholder=""> 
						</div>
						<div class="col-md-3">
							<br>
							<label>Kategori </label> 
							<select class="form-control select2" name="id_kategori" id="id_kategori">
							</select>
						</div>
						<div class="col-md-3">
							<br>
							<label>Penerbit </label> 
							<select class="form-control select2" name="id_penerbit" id="id_penerbit">
							</select>
						</div>
						<div class="col-md-12">
						</div>
						<div class="col-md-6">
							<br>
							<label>Pengarang </label> 
							<input name="pengarang" id="pengarang" type="text" class="form-control" placeholder=""> 
						</div>
						<div class="col-md-2">
							<br>
							<label>Halaman </label> 
							<input class="form-control" name="halaman" id="halaman" type="number" value="">
						</div>
						<div class="col-md-2">
							<br>
							<label>Jumlah </label> 
							<input name="jumlah" id="jumlah" type="number" class="form-control" placeholder=""> 
						</div> 
						<div class="col-md-2">
							<br>
							<label>Tahun Terbit </label> 
							<input class="form-control " name="tahun_terbit" id="tahun_terbit" type="text" value="">
						</div>
					</div>
				</div>
				<div class="modal-footer modal-footer-action">
					<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn green crud-submit">Save changes</button>
				</div> 
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div> 

<!--DOC: Modal Detail--> 
<div class="modal fade" id="modal-detail" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="form-horizontal" id="form-action" action="javascript:void(0);" onSubmit="create()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Data buku</h4>
				</div>
				<div class="modal-body">
					<div class="form-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-xs-3 ">Judul </div>
								<div class="col-xs-9">
									<span id="d_judul"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Kategori </div>
								<div class="col-xs-9">
									<span id="d_nama_kategori"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Penerbit </div>
								<div class="col-xs-9">
									<span id="d_nama_penerbit"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Pengarang </div>
								<div class="col-xs-9">
									<span id="d_pengarang"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Halaman </div>
								<div class="col-xs-9">
									<span id="d_halaman"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Jumlah </div>
								<div class="col-xs-9">
									<span id="d_jumlah"></span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="col-xs-3 ">Tahun Terbit </div>
								<div class="col-xs-9">
									<span id="d_tahun_terbit"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div> 
<script type="text/javascript">  

	$( document ).ready(function() {
		$('#id_kategori').select2({
			tags: false,
			dropdownParent: $("#modal-action"),
			width: '100%', 
			placeholder: "-- Pilih Kategori --",
			ajax : {
				url: url + 'api/get_select2.php',
				dataType: 'json',
				type: 'post',
				delay: 250,
				data: function(params) {
        			// console.log(params);
        			return {
        				table_name : 'kategori',
        				kolom_search : 'nama_kategori',
        				id : 'id_kategori',
        				text : 'nama_kategori',
        				q: params.term || '', // search term
        				page_limit: 10,
        				page: params.page || 1
        			};
        		},
        		processResults: function(data, params) {
        			return {
        				results: data.items,
        				'pagination': {
        					'more': data.more
        				}
        			};
        		},
        		cache: true,
        	}
        });

		$('#id_penerbit').select2({
			tags: false,
			dropdownParent: $("#modal-action"),
			width: '100%', 
			placeholder: "-- Pilih Kategori --",
			ajax : {
				url: url + 'api/get_select2.php',
				dataType: 'json',
				type: 'post',
				delay: 250,
				data: function(params) {
        			// console.log(params);
        			return {
        				table_name : 'penerbit',
        				kolom_search : 'nama_penerbit',
        				id : 'id_penerbit',
        				text : 'nama_penerbit',
        				q: params.term || '', // search term
        				page_limit: 10,
        				page: params.page || 1
        			};
        		},
        		processResults: function(data, params) {
        			return {
        				results: data.items,
        				'pagination': {
        					'more': data.more
        				}
        			};
        		},
        		cache: true,
        	}
        });

		datatable = $('#t_buku').dataTable({
			"bProcessing": true,
			"serverSide": true,
			"ajax": {
				"url": url+'api/getData.php',
				"type": "post",
				"data":{
					table_info : {
						table_name : 'buku', 
						order_by : 'judul', 
						id : 'id_buku',
					},
					column :{
						0 : 'judul', 
						1 : 'nama_kategori', 
						2 : 'nama_penerbit', 
					},
					join :{
						0:{
							table_name 	: 'kategori', 
							condition 	: ' LEFT JOIN kategori ON kategori.id_kategori = buku.id_kategori',
							column 		: 'nama_kategori' 
						},
						1:{
							table_name 	: 'penerbit', 
							condition 	: ' LEFT JOIN penerbit ON penerbit.id_penerbit = buku.id_penerbit',
							column 		: 'nama_penerbit' 
						}
					}
				},
			},
            error: function () {  // error handling code
            	$("#t_buku").css("display", "none");
            }
        });
	}); 

	$("#btn-add").click(function(e){
		$("input[type=text], textarea").val("");
		$('.modal-footer-action').empty();
		$('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="create()">Create</button>');
		$('#form-action').attr('onSubmit','create()'); 
	});
	/* Create new Item */
	function create() {  
		// console.log(namas);
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/create.php',
			data:{
				table_name      : 'buku', 
				order_by        : 'nama_buku', 
				judul      		: $('[name="judul"]').val(),
				id_kategori   	: $('[name="id_kategori"]').val(),
				id_penerbit   	: $('[name="id_penerbit"]').val(),
				pengarang      	: $('[name="pengarang"]').val(),
				halaman    		: $('[name="halaman"]').val(),
				jumlah       	: $('[name="jumlah"]').val(),
				tahun_terbit   	: $('[name="tahun_terbit"]').val(),
			},
			timeout: 10000,
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan, 'Success');
					datatable.api().ajax.reload(null, false);
					$(".modal").modal('hide');
					$("input[type=text], textarea, option, input[type=number]").val("");
				} else {
					toastr.warning(data.pesan, 'Warning');
				}
			},
			error: function () {
				datatable.api().ajax.reload(null, false);
				toastr.warning('An error occurred while connecting to the server.', 'Warning');
			}
		});
	}; 


	/* CUpdate */
	function update(id_buku) { 
		$('.modal-footer-action').empty();
		$('.modal-footer-action').append('<button type="button" class="btn dark btn-outline pull-left" data-dismiss="modal">Close</button><button type="button" class="btn green crud-submit" onclick="proses_update()">Update</button>');
		$('#form-action').attr('onSubmit','proses_update()');  
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/get_update_data.php',
			data:{
				table_name  : 'buku', 
				id_buku    : id_buku,
				join :{
					0:{
						table_name 	: 'kategori', 
						condition 	: ' LEFT JOIN kategori ON kategori.id_kategori = buku.id_kategori',
						column 		: 'nama_kategori' 
					},
					1:{
						table_name 	: 'penerbit', 
						condition 	: ' LEFT JOIN penerbit ON penerbit.id_penerbit = buku.id_penerbit',
						column 		: 'nama_penerbit' 
					}
				}
			},
			success: function (data) { 
				$('[name="id_buku"]').val(data.id_buku);
				$('[name="judul"]').val(data.judul); 
				$('[name="nisn"]').val(data.nisn);
				$('[name="id_kategori"]').append('<option value="'+data.id_kategori+'">'+data.nama_kategori+'</option>');
				$('[name="id_kategori"]').val(data.id_kategori).trigger("change");
				$('[name="id_penerbit"]').append('<option value="'+data.id_penerbit+'">'+data.nama_penerbit+'</option>');
				$('[name="id_penerbit"]').val(data.id_penerbit).trigger("change");
				$('[name="pengarang"]').val(data.pengarang);
				$('[name="halaman"]').val(data.halaman);
				$('[name="jumlah"]').val(data.jumlah);
				$('[name="tahun_terbit"]').val(data.tahun_terbit);
			}
		})
	}; 

	/* Proses Update */
	function proses_update() {   
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/update.php',
			data:{
				table_name      : 'buku', 
				order_by        : 'nama_buku', 
				id_buku        : $('[name="id_buku"]').val(), 
				judul      		: $('[name="judul"]').val(),
				id_kategori   	: $('[name="id_kategori"]').val(),
				id_penerbit   	: $('[name="id_penerbit"]').val(),
				pengarang      	: $('[name="pengarang"]').val(),
				halaman    		: $('[name="halaman"]').val(),
				jumlah       	: $('[name="jumlah"]').val(),
				tahun_terbit   	: $('[name="tahun_terbit"]').val(),
			},
			timeout: 10000,
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan, 'Success');
					datatable.api().ajax.reload(null, false);
					$(".modal").modal('hide');
					$("input[type=text], textarea, option, input[type=number]").val("");
				} else {
					toastr.warning(data.pesan, 'Galat');
				}
			},
			error: function () {
				datatable.api().ajax.reload(null, false);
				toastr.warning('An error occurred while connecting to the server.', 'Galat');
			}
		}); 
	}; 


	/* Remove Item */
	function delete_data(id_buku) {
		var delete_data=confirm('Are you sure to delete this data?');
		if (delete_data) {  
			var form_action = 'api/delete.php'; 
			var table_name = 'buku'; 

			$.ajax({
				dataType: 'json',
				type:'POST',
				url: url + form_action,
				data:{table_name : table_name, order_by : 'nama_buku', id_buku:id_buku},
				timeout: 10000,
				success: function (data) {
					if (data.status) {
						toastr.success(data.pesan, 'Success');
						datatable.api().ajax.reload(null, false);
						$(".modal").modal('hide');
					} else {
						toastr.warning(data.pesan, 'Galat');
					}
				},
				error: function () {
					datatable.api().ajax.reload(null, false);
					toastr.warning('An error occurred while connecting to the server.', 'Galat');
				}
			})

		}
	}; 

	/* CUpdate */
	function detail(id_buku) {    
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: url + 'api/get_update_data.php',
			data:{
				table_name : 'buku', 
				id_buku:id_buku,
				join :{
					0:{
						table_name 	: 'kategori', 
						condition 	: ' LEFT JOIN kategori ON kategori.id_kategori = buku.id_kategori',
						column 		: 'nama_kategori' 
					},
					1:{
						table_name 	: 'penerbit', 
						condition 	: ' LEFT JOIN penerbit ON penerbit.id_penerbit = buku.id_penerbit',
						column 		: 'nama_penerbit' 
					}
				}
			},
			success: function (data) {  
				$('#d_judul').html(data.judul); 
				$('#d_nama_kategori').html(data.nama_kategori);
				$('#d_nama_penerbit').html(data.nama_penerbit);
				$('#d_pengarang').html(data.pengarang);
				$('#d_halaman').html(data.halaman);
				$('#d_jumlah').html(data.jumlah);
				$('#d_tahun_terbit').html(data.tahun_terbit);
			}
		})
	}; 
</script>

<?php include '../template/admin/footer.php'; ?>