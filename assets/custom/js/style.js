
var form = document.querySelector('.search-form');

form.onsubmit = function(e) {
	e.preventDefault();
	form.classList.add('loading'); 
	setTimeout(function(){
		form.classList.remove('loading');
		document.body.classList.add('show');
	}, 1000);
	return false;
};


function get_alamat_user() { 
	container = $('#modal-ubah-alamat-user .box-body');
	container.preventDefault();
	container.html('<div class="text-center text-white" style="font-size: 20px;"><i class="fas fa-spinner"></i> Loading Data...</div>');
	$('#modal-ubah-alamat-user').modal('show');
	$.ajax({
		url: "<?php echo base_url('troli/get_alamat_user') ?>",
		type: 'POST',
		timeout: 5000,
		success: function(data) {
			container.html(data); 
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$('#modal-ubah-alamat-user').modal('hide');
			if (jqXHR.status == '403') {
				window.location.href = base_url;
			}
			toastr.error('Terjadi kesalahan saat menghubungkan ke server.');
		}
	});
} ;
var html= '\
<div style="margin-bottom: 50px;">\
<button type="button" class="close " data-dismiss="modal" aria-label="Close">\
<span class=" cl-white">&times;</span>\
</button>\
</div>\
<div class="row" style="margin-bottom: 30px;">\
<div class="text-center col-md-4">\
<i class="fas fa-id-card-alt fa-5x cl-white"></i>\
<br>\
<br>\
</div>\
<div class="col-md-8 cl-white text-center">\
<br>\
Nama : Artour Baberv\
<br>\
Jumlah Buku Pinjaman : 0\
</div>\
</div>\
<div class="form-group"> \
<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter ID">\
<small id="emailHelp" class="form-text cl-white">Silakan isi id untuk memeriksa pinjaman.</small>\
</div>';
$( '#tombol-periksa' ).click(function() {
	container = $('#exampleModal .modal-body');
	container.html("<div class='text-center text-white' style='font-size: 20px;'><i class='fas fa-spinner fa-spin'></i> Loading Data...</div>");
	setTimeout(function(){
		container.html(html);
	}, 1000);
});