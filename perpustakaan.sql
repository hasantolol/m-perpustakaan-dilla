/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.21-MariaDB : Database - perpustakaan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`perpustakaan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `perpustakaan`;

/*Table structure for table `buku` */

DROP TABLE IF EXISTS `buku`;

CREATE TABLE `buku` (
  `id_buku` varchar(32) NOT NULL,
  `judul` varchar(225) DEFAULT NULL,
  `id_kategori` varchar(32) DEFAULT NULL,
  `id_penerbit` varchar(32) DEFAULT NULL,
  `pengarang` varchar(225) DEFAULT NULL,
  `halaman` int(5) DEFAULT NULL,
  `jumlah` int(4) DEFAULT NULL,
  `tahun_terbit` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id_buku`),
  KEY `fk_buku_1` (`id_kategori`),
  KEY `fk_buku_2` (`id_penerbit`),
  CONSTRAINT `fk_buku_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`),
  CONSTRAINT `fk_buku_2` FOREIGN KEY (`id_penerbit`) REFERENCES `penerbit` (`id_penerbit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `buku` */

insert  into `buku`(`id_buku`,`judul`,`id_kategori`,`id_penerbit`,`pengarang`,`halaman`,`jumlah`,`tahun_terbit`) values ('1','buku contoh','1fb1b8c16b49460faf4f21a53770407e','c004d6846ae7404e8df7c4a9a058fad9','1',1,1,'1'),('9a3ddd8bafc44b23a84172bd29c373ba','test buku','1fb1b8c16b49460faf4f21a53770407e','c004d6846ae7404e8df7c4a9a058fad9','test pengarang',12,1,'2019');

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` varchar(32) NOT NULL,
  `nama_kategori` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kategori`,`nama_kategori`) values ('1fb1b8c16b49460faf4f21a53770407e','Horor'),('525017b13f4e4836bedac242399766b1','scifi'),('77fa622ac89643e489e4513977a3ad51','Romance'),('7cb2e66289d94e9a88d7f839e36079c3','Komedi');

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

CREATE TABLE `peminjaman` (
  `id_peminjaman` varchar(32) NOT NULL,
  `nomor_pinjam` varchar(255) DEFAULT NULL,
  `tanggal_pinjam` datetime DEFAULT NULL,
  `id_siswa` varchar(32) DEFAULT NULL,
  `keterangan` text,
  `lama_pinjam` int(2) DEFAULT '7',
  `status` enum('borrowed','returned') DEFAULT NULL,
  `id_user` varchar(32) DEFAULT '0',
  PRIMARY KEY (`id_peminjaman`),
  KEY `fk_peminjaman_1` (`id_siswa`),
  KEY `fk_peminjaman_2` (`id_user`),
  CONSTRAINT `fk_peminjaman_1` FOREIGN KEY (`id_siswa`) REFERENCES `siswa` (`id_siswa`),
  CONSTRAINT `fk_peminjaman_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `peminjaman` */

insert  into `peminjaman`(`id_peminjaman`,`nomor_pinjam`,`tanggal_pinjam`,`id_siswa`,`keterangan`,`lama_pinjam`,`status`,`id_user`) values ('6f65850726b54af98e5453a152fd5c78','12312','2019-06-30 00:00:00','025952c9f7134e978f85ec11c4f60e8e','ewqeqw',7,'returned','0');

/*Table structure for table `peminjaman_item` */

DROP TABLE IF EXISTS `peminjaman_item`;

CREATE TABLE `peminjaman_item` (
  `id_peminjaman` varchar(255) NOT NULL,
  `id_buku` varchar(32) DEFAULT NULL,
  `jumlah` int(2) DEFAULT NULL,
  KEY `fk_peminjaman_item_2` (`id_buku`),
  KEY `fk_peminjaman_item_1` (`id_peminjaman`),
  CONSTRAINT `fk_peminjaman_item_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`),
  CONSTRAINT `fk_peminjaman_item_2` FOREIGN KEY (`id_buku`) REFERENCES `buku` (`id_buku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `peminjaman_item` */

insert  into `peminjaman_item`(`id_peminjaman`,`id_buku`,`jumlah`) values ('6f65850726b54af98e5453a152fd5c78','1',2);

/*Table structure for table `penerbit` */

DROP TABLE IF EXISTS `penerbit`;

CREATE TABLE `penerbit` (
  `id_penerbit` varchar(32) NOT NULL,
  `nama_penerbit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_penerbit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `penerbit` */

insert  into `penerbit`(`id_penerbit`,`nama_penerbit`) values ('124b23af56854624a19836d76d38cf49','test penerbit'),('c004d6846ae7404e8df7c4a9a058fad9','Arya');

/*Table structure for table `pengadaan` */

DROP TABLE IF EXISTS `pengadaan`;

CREATE TABLE `pengadaan` (
  `id_pengadaan` varchar(32) NOT NULL,
  `tanggal_pengadaan` datetime DEFAULT NULL,
  `id_buku` varchar(32) DEFAULT NULL,
  `asal_buku` varchar(255) DEFAULT NULL,
  `jumlah` int(4) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id_pengadaan`),
  KEY `fk_pengadaan_1` (`id_buku`),
  CONSTRAINT `fk_pengadaan_1` FOREIGN KEY (`id_buku`) REFERENCES `buku` (`id_buku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengadaan` */

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `id_pengembalian` varchar(32) NOT NULL,
  `nomor_kembali` varchar(255) DEFAULT NULL,
  `id_peminjaman` varchar(255) DEFAULT NULL,
  `tanggal_kembali` datetime DEFAULT NULL,
  `denda` decimal(14,2) DEFAULT NULL,
  `id_user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_pengembalian`),
  KEY `fk_pengembalian_1` (`id_peminjaman`),
  KEY `fk_pengembalian_2` (`id_user`),
  CONSTRAINT `fk_pengembalian_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`),
  CONSTRAINT `fk_pengembalian_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pengembalian` */

/*Table structure for table `siswa` */

DROP TABLE IF EXISTS `siswa`;

CREATE TABLE `siswa` (
  `id_siswa` varchar(32) NOT NULL,
  `nama_siswa` varchar(255) DEFAULT NULL,
  `nisn` varchar(40) DEFAULT NULL,
  `jenis_kelamin` enum('Laki - Laki','Perempuan') DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `siswa` */

insert  into `siswa`(`id_siswa`,`nama_siswa`,`nisn`,`jenis_kelamin`,`agama`,`tempat_lahir`,`tanggal_lahir`,`telephone`,`alamat`,`foto`) values ('025952c9f7134e978f85ec11c4f60e8e','Adilla Gustiarani','322','Perempuan','Islam','wonosobo','1997-08-13','08321263721','jambusari','3'),('bcfa7d74bc3047e294aeaa45b9cf41a1','Iste voluptatem libe','Nihil doloribus fuga','Perempuan','Kristen Protestan','Nostrud omnis omnis ','0000-00-00','+1 (963) 788-6511','Voluptas fugiat comm',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` varchar(32) NOT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama_user`,`username`,`password`) values ('0','admin','admin','admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
