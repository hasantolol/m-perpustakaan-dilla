<?php $page = 'home';?>
<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<!-- <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->

	<link rel="stylesheet" type="text/css" href="assets/custom/css/style.css">
	<!-- <link rel="stylesheet" type="text/css" href="assets/fa/css/all.css">  -->
	<!-- <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body bg-black-lighten-2">
					<div style="margin-bottom: 50px;">
						<button type="button" class="close " data-dismiss="modal" aria-label="Close">
							<span class=" cl-white">&times;</span>
						</button>
					</div>
					<div class="text-center" style="margin-bottom: 30px;">
						<i class="fas fa-id-card-alt fa-5x cl-white"></i>
						<br>
						<br>
					</div>
					<div class="form-group"> 
						<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter ID">
						<small id="emailHelp" class="form-text cl-white">Silakan isi ID untuk memeriksa pinjaman.</small>
					</div>  
				</div>
				<div class="row justify-content-center" style="padding: 30px 0px;"> 
					<button type="button" class="btn btn-secondary" id="tombol-periksa">Periksa</button>
				</div>
			</div>
		</div>
	</div>
	<div class="body">
		<div class="align-baseline check-button" style="">
			<button type="button" class="btn float-right bg-black-lighten-1 cl-white" data-toggle="modal" data-target="#exampleModal">Pinjaman Saya</button>
		</div>
		<header>
			<div class="text-center">
				<img src="assets/images/search-logo.png" class="logo">
			</div>
			<form class="search-form">
				<input type="text" class="search" placeholder="Cari disini..." autofocus autocomplete="off" />
				<button class="submit-btn" type="submit"></button>
			</form>
		</header>

		<main>
			<div class="search-results" style="padding-bottom: 200px;">
				<section class="search-result row">
					<div class="col-md-4 text-center">
						<img src="assets/images/download.jpg" class="search-result-image"> 
					</div>
					<div class="col-md-8">
						<h3>
							<a href="#">
								Judul Buku
							</a>
						</h3>
						<p>
							Penerbit : Pengarang
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat...
						</p>
						<i>
							tersedia : 8 buku
						</i>
						<div class="text-right">
							<small>
								Rak 2B
							</small>
						</div>
					</div>
				</section>
				<section class="search-result row">
					<div class="col-md-4 text-center">
						<img src="assets/images/download.jpg" class="search-result-image"> 
					</div>
					<div class="col-md-8">
						<h3>
							<a href="#">
								Judul Buku
							</a>
						</h3>
						<p>
							Penerbit : Pengarang
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat...
						</p>
						<i>
							tersedia : 8 buku
						</i>
						<div class="text-right">
							<small>
								Rak 2B
							</small>
						</div>
					</div>
				</section>
				<section class="search-result row">
					<div class="col-md-4 text-center">
						<img src="assets/images/download.jpg" class="search-result-image"> 
					</div>
					<div class="col-md-8">
						<h3>
							<a href="#">
								Judul Buku
							</a>
						</h3>
						<p>
							Penerbit : Pengarang
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat...
						</p>
						<i>
							tersedia : 8 buku
						</i>
						<div class="text-right">
							<small>
								Rak 2B
							</small>
						</div>
					</div>
				</section>
				<section class="search-result row">
					<div class="col-md-4 text-center">
						<img src="assets/images/download.jpg" class="search-result-image"> 
					</div>
					<div class="col-md-8">
						<h3>
							<a href="#">
								Judul Buku
							</a>
						</h3>
						<p>
							Penerbit : Pengarang
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat...
						</p>
						<i>
							tersedia : 8 buku
						</i>
						<div class="text-right">
							<small>
								Rak 2B
							</small>
						</div>
					</div>
				</section>
				<section class="search-result row">
					<div class="col-md-4 text-center">
						<img src="assets/images/download.jpg" class="search-result-image"> 
					</div>
					<div class="col-md-8">
						<h3>
							<a href="#">
								Judul Buku
							</a>
						</h3>
						<p>
							Penerbit : Pengarang
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat...
						</p>
						<i>
							tersedia : 8 buku
						</i>
						<div class="text-right">
							<small>
								Rak 2B
							</small>
						</div>
					</div>
				</section>

			</div>
		</main>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="assets/custom/js/style.js" ></script>
</body>
</html>