<?php


require 'db_config.php';
function get_uuid() {
	return sprintf( '%04x%04x%04x%04x%04x%04x%04x%04x',
        // 32 bits for "time_low"
		mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
		mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
		mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
		mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
		mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	);
} 

$post_data=[];
parse_str($_POST['data'],$post_data);

$post_data['id_'.$_POST['table_name']] =  get_uuid();

$columns = implode(", ",array_keys($post_data)); 
$values  = implode("', '", array_values($post_data)); 
$sql = "INSERT INTO `".$_POST['table_name']."` ($columns) VALUES ('$values') ; ";

/*start olah data batch*/
if (isset($_POST['batch']) AND !empty($_POST['batch'])) {

	$post_batch=[];
	parse_str($_POST['batch'],$post_batch);

	$sql_batch_tmp1 = array(); 
	$batch_columns = implode(", ",array_keys($post_batch)); 

	foreach( $post_batch as $key => $val) {
		if (!empty($val)) {
			foreach ($val as $keys => $vals) {
				$sql_batch_tmp1[$keys][$key] =!empty($vals)?$vals:$post_data['id_'.$_POST['table_name']];
			}
		}
	}

	foreach ($sql_batch_tmp1 as $key1 => $val1) {
		$sql_batch_tmp2[] = "('".implode("', '", array_values($val1))."')";
	}

	$batch_values = implode(", ", array_values($sql_batch_tmp2));
	$batch_sql	= "INSERT INTO `".$_POST['table_batch_name']."` ($batch_columns) VALUES $batch_values ; ";

	$result = $mysqli->multi_query($sql.$batch_sql);
}else{
	$result = $mysqli->query($sql);
}
/*end olah data batch*/




if($result)
{
	$data = [
		'status' => true,
		'pesan' => 'Data Berhasil Ditambahkan !'
	]; 
}
else
{ 
	$data = [
		'status' => false,
		'pesan' => 'Galat . Error : '.mysqli_error($mysqli)
	];
} 

echo json_encode($data);


?>