<?php


require 'db_config.php'; 

$join_table 	= "";
$column_join 	= "";
if (!empty($_POST['join'])) {
	foreach ($_POST['join'] as $join => $join_value) {
		$join_table .= " ".$join_value['condition']." ";
		$column_join .=", ".$join_value['column']." ";
	}
}

$sql = "SELECT ".$_POST['table_name'].".* $column_join FROM `".$_POST['table_name']."` $join_table WHERE ".$_POST['table_name'].".id_".$_POST['table_name']." = '".$_POST['id_'.$_POST['table_name']]."'";

$result 		= $mysqli->query($sql);
$data['data'] 	= $result->fetch_assoc();

if (isset($_POST['batch']) AND !empty($_POST['batch'])) {

	$join_table_batch = "";
	$column_join_batch = "";
	if (!empty($_POST['batch']['join'])) {
		foreach ($_POST['batch']['join'] as $join_batch => $join_value_batch) {
			$join_table_batch .= " ".$join_value_batch['condition']." ";
			$column_join_batch .=", ".$join_value_batch['column']." ";
		}
	}
	
	$sql_batch = "SELECT ".$_POST['batch']['table_name'].".* $column_join_batch FROM `".$_POST['batch']['table_name']."` $join_table_batch WHERE ".$_POST['batch']['table_name'].".".$_POST['batch']['column']." = '".$_POST['id_'.$_POST['table_name']]."'";

	$result = $mysqli->query($sql_batch);
	$data['batch'] = $result->fetch_all(PDO::FETCH_NUM );

// $data['batch'] = array();
// while ($row = $result->fetch_all(PDO::FETCH_NUM)) {
//     $data['batch'][$row[0]] = $row;
// }
}


// echo "<pre>";
// print_r($sql_batch); 

echo json_encode($data);


?>