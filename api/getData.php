<?php
require 'db_config.php';
$join_table = "";
$column_join = "";
if (!empty($_POST['join'])) {
  foreach ($_POST['join'] as $join => $join_value) {
    $join_table .= " ".$join_value['condition']." ";
    $column_join .=", ".$join_value['column']." ";
  }
}
// echo "<pre>";
// print_r($_POST);
// print_r($join_table);
// print_r($column_join);
$id=$_POST['table_info']['id'];
$order_by = $_POST['order']; // This array contains order information of clicked column
    $search = $_POST['search']['value']; // This array contains search value information datatable
    $start = $_POST['start']; // start limit of data
    $length = $_POST['length']; // end limit of data
    $order_type = $order_by[0]['dir'];
    $order_column='';
    if ($order_by[0]['column']==0){
    	$order_column = $_POST['table_info']['order_by'];
    } elseif ($order_by[0]['column']==1){
    	$order_column = $_POST['table_info']['order_by'];
    }
    // echo "<pre>";
    // print_r($_POST['column'][0]);

    $str = "WHERE (1 = 1) ";
    if (isset($_POST['where']) && ($_POST['where']!='' || $_POST['where']!=NULL)) { 
      $str .= " AND (".$_POST['where']['column']." = '".$_POST['where']['value']."' ) "; 
    };

    if ($search!='' || $search!=NULL) { 
    	$column=0;
    	if (count($_POST['column']) == 1) { 
    		$str .= " AND ".$_POST['column'][$column]." LIKE '%$search%'"; 
    	}else{
    		$str .= " AND ( ".$_POST['column'][$column]." LIKE '%$search%'"; 
    		$column+=1;
    		for ($i=$column; $i < count($_POST['column']); $i++) { 
    			$str .= " OR ".$_POST['column'][$i]." LIKE '%$search%' ";
    		} 

        $str .= " ) "; 
    	} 
    } 
    $data[][] = array(); 
    $i = 0;
    $nomor = 1;
    $sql = $mysqli->query("SELECT ".$_POST['table_info']['table_name'].".* $column_join FROM ".$_POST['table_info']['table_name']." $join_table $str ORDER BY $order_column $order_type LIMIT $start,$length"); 
    // echo "SELECT * $column_join FROM ".$_POST['table_info']['table_name']." $join_table $str ORDER BY $order_column $order_type LIMIT $start,$length";
    // echo "<pre>";
    // echo "SELECT ".$_POST['table_info']['table_name'].".* $column_join FROM ".$_POST['table_info']['table_name']." $join_table $str ORDER BY $order_column $order_type LIMIT $start,$length";
    while ($row_1 = $sql->fetch_assoc()){
    	$data[$i] = array();

      $data[$i][] = $nomor.'.'; 
      for ($s=0; $s < count($_POST['column']); $s++) {
        $data[$i][] = $row_1[$_POST["column"][$s]]; 
      } 
      $data[$i][] = '<center><button data-toggle="modal" data-target="#modal-detail" onclick="detail(\''.$row_1[$_POST['table_info']['id']].'\')" class="btn btn-info"><i class="fa fa-eye"></i></button><button data-toggle="modal" data-target="#modal-action" onclick="update(\''.$row_1[$_POST['table_info']['id']].'\')" class="btn btn-primary"><i class="fa fa-edit"></i></button><button class="btn btn-danger" onclick="delete_data(\''.$row_1[$_POST['table_info']['id']].'\')""><i class="fa fa-trash"></i></button><center>'; 

      $i++;
      $nomor++;
    } 
    $sql_2 = $mysqli->query("SELECT COUNT(*) AS all_count FROM ".$_POST['table_info']['table_name']." $join_table $str");
    $row_2 = $sql_2->fetch_assoc();
    $totalRecords = $row_2['all_count'];
    if ($totalRecords==0){
     $data = array();
   }
   $json_data = array(
     "draw"            => intval( $_POST['draw'] ),
     "recordsTotal"    => intval( $totalRecords ),
     "recordsFiltered" => intval($totalRecords),
    "data"            => $data   // total data array
  );
   $json_data = json_encode($json_data);
   echo $json_data;
   ?>