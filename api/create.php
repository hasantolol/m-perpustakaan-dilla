<?php


require 'db_config.php';
function get_uuid() {
	return sprintf( '%04x%04x%04x%04x%04x%04x%04x%04x',
        // 32 bits for "time_low"
		mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
		mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
		mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
		mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
		mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	);
}
$post = $_POST;
unset($post['table_name']);
unset($post['order_by']);
$post['id_'.$_POST['table_name']] =  get_uuid();

$columns = implode(", ",array_keys($post)); 
$values  = implode("', '", array_values($post)); 
$sql = "INSERT INTO `".$_POST['table_name']."` ($columns) VALUES ('$values')";

$result = $mysqli->query($sql);



if($result)
{
	$data = [
		'status' => true,
		'pesan' => 'Data Berhasil Ditambahkan !'
	]; 
}
else
{ 
	$data = [
		'status' => false,
		'pesan' => 'Galat . Error : '.mysqli_error($mysqli)
	];
} 

echo json_encode($data);


?>