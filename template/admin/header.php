<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
  <meta charset="utf-8" />
  <title>Perpustakaanku</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
  <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="../assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
  <link href="../assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- END THEME LAYOUT STYLES -->
  <link rel="shortcut icon" href="favicon.ico" /> </head>
  <!-- END HEAD -->

  <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
  <script src="../assets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script> -->
  <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript">
    var url = "http://localhost/perpustakaan-oke/";
  </script>
  <?php 
  $url = "http://localhost/perpustakaan-oke/";
  ?>
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
      <!-- BEGIN HEADER -->
      <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
          <!-- BEGIN LOGO -->
          <div class="page-logo">
            <a href="index.php">
              <img src="../assets/layouts/layout/img/logo-perpus.png" alt="logo" class="logo-default" /> </a>
              <div class="menu-toggler sidebar-toggler">
                <span></span>
              </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
              <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
              <ul class="nav navbar-nav pull-right">   
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img alt="" class="img-circle" src="../assets/layouts/layout/img/avatar3_small.jpg" />
                    <span class="username username-hide-on-mobile"> Nick </span>
                    <i class="fa fa-angle-down"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-default">
                    <li>
                      <a href="page_user_profile_1.html">
                        <i class="icon-user"></i> My Profile </a>
                      </li> 
                      <li class="divider"> </li> 
                      <li>
                        <a href="page_user_login_1.html">
                          <i class="icon-key"></i> Log Out </a>
                        </li>
                      </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                      <a href="javascript:;" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                      </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                  </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
              </div>
              <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
              <!-- BEGIN SIDEBAR -->
              <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                  <!-- BEGIN SIDEBAR MENU -->
                  <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                  <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                  <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                  <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                  <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                  <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                  <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <li class="sidebar-toggler-wrapper hide">
                      <div class="sidebar-toggler">
                        <span></span>
                      </div>
                    </li>
                    <!-- END SIDEBAR TOGGLER BUTTON -->
                    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->  
                    <li class="nav-item <?php if($page==='home'){echo 'open active';}?>">
                      <a href="index.php" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                      </a> 
                    </li>
                    <li class="nav-item<?php if($page==='user' || $page==='penerbit' || $page==='buku' || $page==='siswa' || $page==='kategori'){echo 'open active';}?>">
                      <a href="javascript;" class="nav-link nav-toggle">
                        <i class="icon-globe"></i>
                        <span class="title">General</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                      </a>
                      <ul class="sub-menu"> 
                        <li class="nav-item <?php if($page==='user'){echo 'open active';}?> ">
                          <a href="user.php" class="nav-link ">
                            <span class="title">User</span>
                          </a>
                        </li>
                        <li class="nav-item <?php if($page==='penerbit'){echo 'open active';}?> ">
                          <a href="penerbit.php" class="nav-link ">
                            <span class="title">Penerbit</span>
                          </a>
                        </li>
                        <li class="nav-item <?php if($page==='buku'){echo 'open active';}?> ">
                          <a href="buku.php" class="nav-link ">
                            <span class="title">Buku</span>
                          </a>
                        </li>
                        <li class="nav-item <?php if($page==='kategori'){echo 'open active';}?> ">
                          <a href="kategori.php" class="nav-link ">
                            <span class="title">Kategori Buku</span>
                          </a>
                        </li> 
                        <li class="nav-item <?php if($page==='siswa'){echo 'open active';}?> ">
                          <a href="siswa.php" class="nav-link ">
                            <span class="title">Siswa</span>
                          </a>
                        </li> 
                      </ul>
                    </li>
                    <li class="nav-item <?php if($page==='peminjaman_buku'){echo 'open active';}?>">
                      <a href="peminjaman_buku.php" class="nav-link nav-toggle">
                        <i class="icon-layers"></i>
                        <span class="title">Peminjaman</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                      </a> 
                    </li>
                    <li class="nav-item"<?php if($page==='pengembalian_buku'){echo 'open active';}?>>
                      <a href="pengembalian_buku.php" class="nav-link nav-toggle">
                        <i class="icon-paper-plane"></i>
                        <span class="title">Pengembalian</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                      </a> 
                    </li>
                    <li class="nav-item"<?php if($page==='pengadaan'){echo 'open active';}?>>
                      <a href="pengadaan.php" class="nav-link nav-toggle">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Pengadaan</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                      </a> 
                    </li>
                  </ul>
                  <!-- END SIDEBAR MENU -->
                  <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
              </div>
              <!-- END SIDEBAR -->
              <!-- BEGIN CONTENT -->
              <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                  <!-- BEGIN PAGE HEADER-->
                  <!-- BEGIN PAGE BAR -->
                  <div class="page-bar">
                    <ul class="page-breadcrumb">
                      <li>
                        <a href="<?= $url.'admin/';?>">Perpustakaan</a>
                        <i class="fa fa-circle"></i>
                      </li>
                      <?php 
                      if (!empty($page)) {
                        ?>
                        <li>
                          <span><?= ucwords(str_replace("_", " ", $page));?></span>
                        </li>
                      <?php };?>
                    </ul> 
                  </div>
                  <!-- END PAGE BAR -->
                  <!-- END PAGE HEADER-->
                        <!-- START ISI_KONTEN -->